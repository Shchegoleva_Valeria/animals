package ru.svs.animals;

public class Cat extends Animal {

    public Cat () {
        this("Кошка", "мяу-мяу");
    }


    protected Cat(String name, String voice) {
        super(name, voice);
    }
}