package ru.svs.animals;

public class Main {

    public static void main(String... args) {
        Dog dog = new Dog();
        Dog dog1 = new Dog("Собака", "ав-ав");
        Dog dog2 = new Dog("Тузик", "тяф-тяф");

        dog.printDisplay();
        dog1.printDisplay();
        dog2.printDisplay();


        Cat cat = new Cat();
        Cat cat1 = new Cat("Кот", "мяу");
        Cat cat2 = new Cat("Барсик", "мяууу");

        cat.printDisplay();
        cat1.printDisplay();
        cat2.printDisplay();


        Hedgehog hedgehog = new Hedgehog();
        Hedgehog hedgehog1 = new Hedgehog("Ежик", "фырр-фыр");
        Hedgehog hedgehog2 = new Hedgehog("Дмитрий", "фыррр-фырр");

        hedgehog.printDisplay();
        hedgehog1.printDisplay();
        hedgehog2.printDisplay();

    }
}
