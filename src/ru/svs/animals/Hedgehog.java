package ru.svs.animals;


public class Hedgehog extends Animal {

    public Hedgehog () {
        this("Ежик", "фыр-фыр");
    }


    protected Hedgehog(String name, String voice) {
        super(name, voice);
    }
}
